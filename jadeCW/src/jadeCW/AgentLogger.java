package jadeCW;

import jade.core.Agent;

/** This class simply logs the details for a specific agent **/
public class AgentLogger {

  private transient final Agent agent;

  public AgentLogger(final Agent agent) {
    this.agent = agent;
  }

  public void log(final String action) {
    System.out.format("%s: %s\n", agent.getLocalName(), action);
  }

  public void format(String format, Object... args) {
    log(String.format(format, args));
  }
}
