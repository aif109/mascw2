package jadeCW;

import static jade.lang.acl.ACLMessage.AGREE;
import static jade.lang.acl.ACLMessage.REFUSE;
import static jade.lang.acl.ACLMessage.REQUEST;
import static jade.lang.acl.MessageTemplate.MatchConversationId;
import static jade.lang.acl.MessageTemplate.MatchPerformative;
import static jade.lang.acl.MessageTemplate.and;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

/**
 * This class deals with the appointment allocation for the agents.
 */

// Step 4a
class AllocateAppointment extends CyclicBehaviour {

  private final HospitalAgent hospitalAgent;

  AllocateAppointment(HospitalAgent hospitalAgent) {
    this.hospitalAgent = hospitalAgent;
  }

  private static final long serialVersionUID = 1L;
  private final MessageTemplate mt = and(
      MatchConversationId(Services.ALLOCATE_APPOINTMENTS),
      MatchPerformative(REQUEST));

  @Override
  public void action() {
    // Receive the request for an appointment
    // If there are any appointments available
    // (i.e. not all appointments have been allocated),
    // then pick one (any) and agree to the request
    // notifying the requesting agent of the appointment
    // allocated to it.
    // In case an appointment is allocated,
    // update the parent agent's state as to which agents
    // have which appointments.
    final ACLMessage request = this.hospitalAgent.receive(mt);

        if (request != null) {
            final ACLMessage reply = request.createReply();
            final int availableIndex = hospitalAgent.getAvailableAppointment();
            
            if (availableIndex > -1) {
                reply.setPerformative(AGREE);
                // Convert the available index into an appointment number
                reply.setContent(String.valueOf(availableIndex + 1));
                hospitalAgent.allocateAppointment(availableIndex, request.getSender());
                hospitalAgent.send(reply);
                
                // broadcast update message to all agents
                hospitalAgent.broadcastUpdate();
            } else {
                // No available appointments, so refuse the request
                reply.setPerformative(REFUSE);
                hospitalAgent.log(hospitalAgent.getLocalName() + " refuses the appointment request from " +
                                request.getSender().getLocalName());
                hospitalAgent.send(reply);
            }
        }

  }

}