package jadeCW;

/**
 * 
 * Appointment specific class
 * 
 */

public class Appointment implements Comparable<Appointment> {
  private Integer number, priority;

  public Appointment(int number, int priority) {
    this.number = number;
    this.priority = priority;
  }

  @Override
  public int compareTo(Appointment o) {
    return priority.compareTo(o.priority);
  }

  public int getNumber() {
    return number;
  }
}
