package jadeCW;

/**
 * This class deals with finding the appointment owner together
 * with finding a better appointment than the currently allocated one.
 */

import static jade.lang.acl.ACLMessage.FAILURE;
import static jade.lang.acl.ACLMessage.INFORM;
import static jade.lang.acl.ACLMessage.REQUEST;
import static jade.lang.acl.MessageTemplate.MatchConversationId;
import static jade.lang.acl.MessageTemplate.MatchInReplyTo;
import static jade.lang.acl.MessageTemplate.and;
import jade.core.AID;
import jade.core.behaviours.Behaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;

import java.util.ArrayList;
import java.util.List;

// STEP 5
class FindAppointmentOwner extends Behaviour {

	private static final long serialVersionUID = 1L;
	private transient int step = 0;
	private transient boolean isDone = false;
	private transient final List<Integer> excludedAppointments = new ArrayList<Integer>();
	private transient int appointmentRequested = 0;
	private transient MessageTemplate msgTemplate;

	private static final int SEND_QUERY = 0;
	private static final int WAIT_REPLY = 1;
	private static final int WAIT_FOR_UPDATES = 2;
	private static final MessageTemplate updateMsgTemplate = and(
			MatchConversationId(Services.UPDATE),
			MessageTemplate.MatchPerformative(ACLMessage.INFORM));

	@Override
	public void action() {
		PatientAgent patientAgent = (PatientAgent) myAgent;
		switch (step) {
		case SEND_QUERY:
			if (!patientAgent.hasAppointment()
					|| !patientAgent.hasAppointmentAgent())
				return;
			// find our allocated appointment in preferences
			final int priority = patientAgent
					.getPriorityOfAllocatedAppointment();

			// Builds shortlist of best appointments
			final List<Integer> shortList = new ArrayList<Integer>();
			for (int i = 0; i < priority; i++) {
				shortList.addAll(patientAgent.getAppointmentsByPriority(i));
			}
			// Remove already checked appointments from shortlist
			shortList.removeAll(excludedAppointments);

			// We have a top priority appointment and so we are done
			if (priority == 0) {
				isDone = true;
				return;
			}

			if (shortList.isEmpty()) {
				step = WAIT_FOR_UPDATES;
				return;
			}

			// Request a better appointment in shortlist
			final ACLMessage msg = new ACLMessage(REQUEST);
			appointmentRequested = shortList.get(0);
			msg.addReceiver(patientAgent.getAppointmentAgent());
			msg.setContent(Integer.toString(appointmentRequested));
			msg.setConversationId(Services.FIND_APPOINTMENT_OWNER);
			msg.setReplyWith(Services.FIND_APPOINTMENT_OWNER
					+ System.currentTimeMillis());
			myAgent.send(msg);
			step = WAIT_REPLY; // go to receive step
			msgTemplate = and(
					MatchConversationId(Services.FIND_APPOINTMENT_OWNER),
					MatchInReplyTo(msg.getReplyWith()));
			break;
		case WAIT_REPLY:
			// Receive response informing the agent as to which agent has
			// the resource, if any, and modify the parent agent's state as
			// such.
			final ACLMessage reply = myAgent.receive(msgTemplate);
			if (reply != null) {
				// Check that we are informed of the appointment allocation
				switch (reply.getPerformative()) {
				case INFORM:
					try {
						// Store the new appointment owner
						patientAgent.updateAppointmentOwner(
								appointmentRequested,
								(AID) reply.getContentObject());
					} catch (final UnreadableException e) {
						e.printStackTrace();
					}
					break;
				case FAILURE:
					patientAgent.log("Failed to find owner of appointment "
							+ appointmentRequested);
					break;
				}
				excludedAppointments.add(appointmentRequested);
				step = SEND_QUERY;
			} else {
				block();
			}
			break;
		case WAIT_FOR_UPDATES:
			final ACLMessage reply2 = myAgent.receive(updateMsgTemplate);
			if (reply2 != null) {
				// when some allocation has changed, search for better apps
				excludedAppointments.clear();
				step = SEND_QUERY;
			} else {
				block();
			}
			break;
		}

	}

	@Override
	public boolean done() {
		return isDone;
	}
}