package jadeCW;

import jade.core.AID;
import jade.core.Agent;
import jade.domain.AMSService;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPANames;
import jade.domain.FIPAAgentManagement.AMSAgentDescription;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.SearchConstraints;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;

import java.util.Arrays;

/**
 * This class deals with the hospital appointments setup and final allocation
 */

public class HospitalAgent extends Agent {

  private static final long serialVersionUID = 1L;
  private int totalNumberOfAppointments = 0;
  private AID[] appointments;

  private final AgentLogger agentLogger = new AgentLogger(this);

  @Override
  public void setup() {

    final Object[] args = getArguments();

    // Get the number of total appointments as a start-up argument

    if (args != null && args.length > 0) {
      try {
        totalNumberOfAppointments = Integer.parseInt((String) args[0]);
        agentLogger.log("The total number of appointments is: "
            + totalNumberOfAppointments);
        appointments = new AID[totalNumberOfAppointments];
      } catch (final Exception e) {
        e.printStackTrace();
      }

      agentLogger.log("Registering service \"hospital\" of type "
          + Services.ALLOCATE_APPOINTMENTS);

      // Register the hospital service of type "allocate-appointments"
      // with the Directory Facilitator
      try {
        final DFAgentDescription dfAgentDescription = new DFAgentDescription();
        dfAgentDescription.setName(getAID());
        final ServiceDescription serviceDescription = new ServiceDescription();
        serviceDescription.setName("hospital");
        serviceDescription.setType(Services.ALLOCATE_APPOINTMENTS);
        serviceDescription.addLanguages(FIPANames.ContentLanguage.FIPA_SL);
        dfAgentDescription.addServices(serviceDescription);
        DFService.register(this, dfAgentDescription);
      } catch (final FIPAException fe) {
        fe.printStackTrace();
      }

      // Add the specified behaviours
      addBehaviour(new AllocateAppointment(this));
      addBehaviour(new RespondToQuery(this));
      addBehaviour(new RespondToProposal2());
      addBehaviour(new UpdateAppointments());

    } else {
      agentLogger.log("No total number of appointments was specified.");
    }
  }

  // --- Step 4b ---
  /**
   * Print all the appointments and their owners when the agent platform is shut
   * down or killed.
   **/
  @Override
  protected void takeDown() {
    int i = 0;
    for (AID aid : appointments) {
      String agentName = aid == null ? "null" : aid.getLocalName();
      agentLogger.format("Appointment %d: %s", ++i, agentName);
    }
  }

  public int getAvailableAppointment() {
    return Arrays.asList(appointments).indexOf(null);
  }

  public void allocateAppointment(int availableIndex, AID sender) {
    appointments[availableIndex] = sender;
    agentLogger.log("Appointment number " + (availableIndex + 1)
        + " being alocated to " + sender.getLocalName() + " by "
        + getLocalName());
  }

  public void log(String action) {
    agentLogger.log(action);
  }

  public boolean knownAppointment(int appointment) {
    return appointment >= 0 && appointment < totalNumberOfAppointments;
  }

  public boolean knownOwner(int appointment) {
    return knownAppointment(appointment) && appointments[appointment] != null;
  }

  public AID getOwnerAID(int appointment) {
    return appointments[appointment];
  }

  public void swapAppointments(int app1, int app2) {
    AID t = appointments[app1];
    appointments[app1] = appointments[app2];
    appointments[app2] = t;
  }

  public void swapAppointments(String agent1, String agent2) {
    int app1 = getAppointment(agent1);
    int app2 = getAppointment(agent2);
    swapAppointments(app1, app2);
  }

  public int getAppointment(String agent) {
    for (int i = 0; i < appointments.length; i++) {
      if (appointments[i] != null
          && appointments[i].getLocalName().equals(agent)) {
        return i;
      }
    }
    return -1;
  }

  public boolean appointmentAvailable(int appointment) {
    return appointments[appointment - 1] == null;
  }

  public void format(String format, Object... args) {
    agentLogger.format(format, args);
  }

  /** Broadcast that a change has occurred */
  public void broadcastUpdate() {
    final ACLMessage notifUpdate = new ACLMessage(ACLMessage.INFORM);
    try {
      SearchConstraints searchConstraints = new SearchConstraints();
      searchConstraints.setMaxDepth(10000l);
      searchConstraints.setMaxResults(10000l);
      for (AMSAgentDescription ad : AMSService.search(this,
          new AMSAgentDescription(), searchConstraints)) {
        notifUpdate.addReceiver(ad.getName());
      }
    } catch (FIPAException e) {
      e.printStackTrace();
    }
    notifUpdate.setConversationId(Services.UPDATE);
    send(notifUpdate);
  }

}
