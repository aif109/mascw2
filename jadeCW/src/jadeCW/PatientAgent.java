package jadeCW;

import static jade.domain.DFService.createSubscriptionMessage;

import static jade.domain.DFService.decodeNotification;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.SearchConstraints;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.proto.SubscriptionInitiator;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * This class deals with the patient appointments setup and their final
 * allocation
 */

public class PatientAgent extends Agent {

  private static final long serialVersionUID = 1L;
  private static final long MAX_SEARCH_RESULTS = 1;

  private transient AID appointmentAgent;
  private transient int allocatedAppointment = 0;

  private int maxPriority;

  private transient final Map<Integer, List<Integer>> appointmentPreferences = new HashMap<Integer, List<Integer>>();
  private transient final Map<Integer, AID> appointmentOwners = new HashMap<Integer, AID>();
  private transient final AgentLogger agentLogger = new AgentLogger(this);

  // --- STEP 2 ---
  @Override
  public void setup() {
    final Object[] args = getArguments();
    int priority = 0;
    if (args != null && args.length > 0) {
      for (final Object arg : args) {
        final String temp = arg.toString();
        if (temp.equals("-")) {
          priority++;
        } else {
          List<Integer> apps = appointmentPreferences.get(priority);
          if (apps == null) {
            apps = new ArrayList<Integer>();
            appointmentPreferences.put(priority, apps);
          }
          apps.add(Integer.parseInt(temp));
        }
      }
    }

    maxPriority = priority;

    addBehaviour(subscribeToAllocateAppointmentsService());
    addBehaviour(new RequestAppointment());
    addBehaviour(new FindAppointmentOwner());
    addBehaviour(new ProposeSwap());
    addBehaviour(new RespondToProposal1());
  }

  private Behaviour subscribeToAllocateAppointmentsService() {
    // Subscribe with DF to be notified of "allocate-appointments" service
    final DFAgentDescription template = new DFAgentDescription();
    final ServiceDescription templateSd = new ServiceDescription();
    templateSd.setType(Services.ALLOCATE_APPOINTMENTS);
    template.addServices(templateSd);

    final SearchConstraints searchConstraints = new SearchConstraints();
    searchConstraints.setMaxResults(MAX_SEARCH_RESULTS);
    ACLMessage subscriptionMessage = createSubscriptionMessage(this,
        getDefaultDF(), template, searchConstraints);
    Behaviour b = new SubscriptionInitiator(this, subscriptionMessage) {

      private static final long serialVersionUID = 1L;

      protected void handleInform(final ACLMessage information) {
        DFAgentDescription[] results;
        try {
          results = decodeNotification(information.getContent());
        } catch (final FIPAException e) {
          e.printStackTrace();
          return;
        }

        if (results != null && results[0] != null) {
          final DFAgentDescription dfAgentDescription = results[0];
          final AID provider = dfAgentDescription.getName();

          final Iterator<?> it = dfAgentDescription.getAllServices();
          while (it.hasNext() && appointmentAgent == null) {
            final ServiceDescription sDescription = (ServiceDescription) it
                .next();
            if (sDescription.getType().equals(Services.ALLOCATE_APPOINTMENTS)) {
              agentLogger.log("Received hospital appointment agent - "
                  + sDescription.getName());
              appointmentAgent = provider;
            }
          }
        }
      }
    };

    return b;
  }

  /** Return the priority of an appointment **/
  public int getPriority(final int appointment) {
    for (Map.Entry<Integer, List<Integer>> entry : appointmentPreferences
        .entrySet()) {
      if (entry.getValue().contains(appointment))
        return entry.getKey();
    }
    return maxPriority;
  }

  // --- Step 4b ---
  /** Output our allocated appointment on being killed */
  @Override
  protected void takeDown() {
    String app = allocatedAppointment > 0 ? "Appointment "
        + allocatedAppointment : null;
    agentLogger.log(app);
  }

  public boolean hasAppointmentAgent() {
    return appointmentAgent != null;
  }

  public AID getAppointmentAgent() {
    return appointmentAgent;
  }

  public boolean hasAppointment() {
    return allocatedAppointment != 0;
  }

  public void setAppointment(String content) {
    allocatedAppointment = Integer.parseInt(content);
    agentLogger.log("Appointment allocated: " + allocatedAppointment);
  }

  public Serializable serializeAppointmentPreferences() {
    return (Serializable) appointmentPreferences;
  }

  public int getPriorityOfAllocatedAppointment() {
    return getPriority(allocatedAppointment);
  }

  public List<Integer> getAppointmentsByPriority(int priority) {
    return appointmentPreferences.get(priority);
  }

  public void updateAppointmentOwner(int appointmentRequested, AID contentObject) {
    appointmentOwners.put(appointmentRequested, contentObject);
    agentLogger.log("Received owner of appointment " + appointmentRequested
        + " [" + contentObject.getLocalName() + " ]");
  }

  public void log(String action) {
    agentLogger.log(action);
  }

  public boolean knowsAgentWithBetterAppointment() {
    return appointmentOwners.size() != 0;
  }

  public List<Swap> getBestPossibleSwaps() {
    List<Swap> bestSwaps = new ArrayList<Swap>();
    for (Integer app : appointmentOwners.keySet())
      bestSwaps
          .add(new Swap(app, getPriority(app), appointmentOwners.get(app)));
    Collections.sort(bestSwaps);
    /*
     * List<Integer> bestApps = new ArrayList<Integer>(); for (Appointment a :
     * possibleApps) { bestApps.add(a.getNumber()); }
     */
    return bestSwaps;
  }

  public AID getOwner(Integer appointment) {
    return appointmentOwners.get(appointment);
  }

  public int getAppointment() {
    return allocatedAppointment;
  }

  public boolean hasBestAppointment() {
    return getPriority(allocatedAppointment) == 0;
  }

  public void setAllocatedAppointment(Integer appointment) {
    allocatedAppointment = appointment;
  }

  public boolean sameOrBetterPriority(int appointment) {
    return getPriority(appointment) <= getPriority(allocatedAppointment);
  }

  public void swapAllocatedAppointment(int appointment) {
    log("Swapping appointment " + allocatedAppointment + " with " + appointment);
    allocatedAppointment = appointment;
  }

  public void format(String format, Object... args) {
    agentLogger.format(format, args);
  }
}
