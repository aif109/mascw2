package jadeCW;

import static jade.lang.acl.MessageTemplate.MatchConversationId;
import static jade.lang.acl.MessageTemplate.MatchInReplyTo;
import static jade.lang.acl.MessageTemplate.and;
import jade.core.AID;
import jade.core.behaviours.Behaviour;
import jade.lang.acl.ACLMessage;
import static jade.lang.acl.ACLMessage.*;
import jade.lang.acl.MessageTemplate;

import java.util.ArrayList;
import java.util.List;

/**
 * Behaviour for the PatientAgent which proposes a swap to any agent it knows of
 * that has a better appointment than the one currently allocated to the parent
 * agent. If swap is successful and not carried out with a hospital agent, then
 * the hospital is informed.
 */

// STEP 7:
public class ProposeSwap extends Behaviour {

  private static final long serialVersionUID = 1L;

  private enum State {
    SEND_SWAP_REQUEST, WAIT_REPLY, WAIT_FOR_UPDATES;
  }

  private State state = State.SEND_SWAP_REQUEST;

  private List<Swap> possibleSwaps = new ArrayList<Swap>();
  private List<Swap> attemptedSwaps = new ArrayList<Swap>();
  private MessageTemplate msgTemplate;
  private int proposedApp;
  private AID owner;
  private Swap proposedSwap;

  private static final MessageTemplate updateMsgTemplate = and(
      MatchConversationId(Services.UPDATE),
      MessageTemplate.MatchPerformative(ACLMessage.INFORM));

  @Override
  public void action() {
    PatientAgent patientAgent = (PatientAgent) myAgent;
    switch (state) {
    case SEND_SWAP_REQUEST:
      if (!patientAgent.knowsAgentWithBetterAppointment())
        break;
      possibleSwaps = patientAgent.getBestPossibleSwaps();
      possibleSwaps.removeAll(attemptedSwaps);
      if (possibleSwaps.isEmpty()) {
        state = State.WAIT_FOR_UPDATES;
        return;
      }
      proposedSwap = possibleSwaps.get(0);
      proposedApp = proposedSwap.getAppointment();
      owner = patientAgent.getOwner(proposedApp);
      SwapMessage msg = new SwapMessage(owner, patientAgent.getAppointment(),
          proposedApp);
      myAgent.send(msg);
      patientAgent.format("Sending request to swap app. %d with %d to '%s'",
          patientAgent.getAppointment(), proposedApp, owner.getLocalName());
      state = State.WAIT_REPLY;
      msgTemplate = and(MatchConversationId(Services.SWAP_APPOINTMENT),
          MatchInReplyTo(msg.getReplyWith()));
      break;
    case WAIT_REPLY:
      final ACLMessage reply = myAgent.receive(msgTemplate);
      if (reply != null) {
        switch (reply.getPerformative()) {
        case ACCEPT_PROPOSAL:
          patientAgent.setAllocatedAppointment(proposedApp);
          if (!isHospitalAgent(owner)) {
            patientAgent
                .format(
                    "Swap proposal accepted by non-hospital agent [%s]. Notifying hospital!",
                    owner.getLocalName());
            myAgent.send(new SwapNotif(patientAgent, owner.getLocalName()));
          } else {
            patientAgent
                .format(
                    "Swap proposal accepted by hospital agent '%s'. No notification required!",
                    owner.getLocalName());
          }
          break;
        case REJECT_PROPOSAL:
          patientAgent.format(
              "Request to swap app. %d with %d rejected by '%s'",
              patientAgent.getAppointment(), proposedApp, owner.getLocalName());
          break;
        }
        attemptedSwaps.add(proposedSwap);
        possibleSwaps.remove(0);
        state = State.SEND_SWAP_REQUEST;
      } else {
        block();
      }
      break;
    case WAIT_FOR_UPDATES:
      final ACLMessage reply2 = myAgent.receive(updateMsgTemplate);
      if (reply2 != null) {
        // when some allocation has changed, search for better apps
        attemptedSwaps.clear();
        state = State.SEND_SWAP_REQUEST;
      } else {
        block();
      }
      break;
    }

  }

  private boolean isHospitalAgent(final AID owner) {
    // we assume there is only one hospital agent, as instructed
    return owner.equals(((PatientAgent) myAgent).getAppointmentAgent());
  }

  @Override
  public boolean done() {
    return ((PatientAgent) myAgent).hasBestAppointment();
  }

}
