package jadeCW;

/**
 * This class deals with the way we request an appointment:
 * 
 * Request an appointment if we don't have one
 * Check that an agent is known that provides the
 * "allocate-appointments" service and
 * Check that this agent (i.e. the parent agent of this
 * behaviour) has not already been allocated an appointment.
 * 
 * Receive hospital's reply indicating whether the appointment
 * has been allocated or not
 * 
 */

import static jade.lang.acl.ACLMessage.AGREE;
import static jade.lang.acl.ACLMessage.REFUSE;
import static jade.lang.acl.MessageTemplate.*;
import jade.core.behaviours.Behaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

import java.io.IOException;

// STEP 3
class RequestAppointment extends Behaviour {

  private static final long serialVersionUID = 1L;
  private int state = 0;
  private final int SEND_REQUEST = 0;
  private final int WAIT_REPLY = 1;
  private MessageTemplate msgTemplate;

  @Override
  public void action() {
    PatientAgent patientAgent = (PatientAgent) myAgent;
    switch (state) {
    case SEND_REQUEST:

      if (patientAgent.hasAppointmentAgent() && !patientAgent.hasAppointment()) {

        final ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
        msg.addReceiver(patientAgent.getAppointmentAgent());
        try {
          // Send our preferences to the hospital
          msg.setContentObject(patientAgent.serializeAppointmentPreferences());
        } catch (final IOException e) {
          e.printStackTrace();
        }
        msg.setConversationId(Services.ALLOCATE_APPOINTMENTS);
        msg.setReplyWith(Services.ALLOCATE_APPOINTMENTS + " "
            + System.currentTimeMillis());
        myAgent.send(msg);

        // Save message template
        msgTemplate = and(MatchConversationId(Services.ALLOCATE_APPOINTMENTS),
            MatchInReplyTo(msg.getReplyWith()));
        state = WAIT_REPLY;
      }
      break;
    case WAIT_REPLY:

      final ACLMessage reply = myAgent.receive(msgTemplate);

      if (reply != null) {
        switch (reply.getPerformative()) {
        case AGREE:
          patientAgent.setAppointment(reply.getContent());
          break;
        case REFUSE:
          patientAgent.setAppointment("0");
          break;
        }
        state = SEND_REQUEST;
      } else {
        block();
      }
      break;
    default:
    }
  }

  @Override
  public boolean done() {
    return ((PatientAgent) myAgent).hasAppointment();
  }
}