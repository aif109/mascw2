package jadeCW;

import static jade.lang.acl.ACLMessage.ACCEPT_PROPOSAL;
import static jade.lang.acl.ACLMessage.REJECT_PROPOSAL;
import static jade.lang.acl.ACLMessage.REQUEST;
import static jade.lang.acl.MessageTemplate.MatchConversationId;
import static jade.lang.acl.MessageTemplate.MatchPerformative;
import static jade.lang.acl.MessageTemplate.and;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

/**
 * Behaviour for the PatientAgent class that receives a swap proposal and
 * accepts iff the proposed app is held by the parent agent and its priority is
 * at least as high as the parent's currently allocated appointment.
 */

// STEP 8:
public class RespondToProposal1 extends CyclicBehaviour {

  private static final long serialVersionUID = 1L;
  private static final MessageTemplate msgTemplate = and(
      MatchConversationId(Services.SWAP_APPOINTMENT),
      MatchPerformative(REQUEST));

  @Override
  public void action() {
    PatientAgent patientAgent = (PatientAgent) myAgent;
    final ACLMessage msg = myAgent.receive(msgTemplate);
    if (msg != null) {
      ACLMessage reply = msg.createReply();
      // decode swap message
      SwapMessage swpMsg = new SwapMessage(msg.getContent());
      int currApp = swpMsg.getCurrentAppointment();
      int proposedApp = swpMsg.getProposedAppointment();
      String sender = msg.getSender().getLocalName();

      // check this agent owns the proposed appointment
      if (patientAgent.getAppointment() != proposedApp) {
        rejectSwap(reply, sender, currApp, proposedApp, "App. not owned");
        return;
      }

      // check the swap is at least as preferred as the current app.
      if (!patientAgent.sameOrBetterPriority(currApp)) {
        rejectSwap(reply, sender, currApp, proposedApp,
            "Lower priority proposed");
        return;
      }

      reply.setPerformative(ACCEPT_PROPOSAL);
      patientAgent.swapAllocatedAppointment(currApp);
      // notify hospital agent of the swap
      myAgent.send(new SwapNotif(patientAgent, sender));
      patientAgent.format(
          "Accept to swap %d with %d proposed by %s. Notifying hospital!",
          proposedApp, currApp, sender);
      myAgent.send(reply);
    } else
      block();
  }

  private void rejectSwap(ACLMessage reply, String sender, int currApp,
      int proposedApp, String msg) {
    ((PatientAgent) myAgent).format(
        "Reject to swap %d with %d proposed by %s: %s!", proposedApp, currApp,
        sender, msg);
    reply.setPerformative(REJECT_PROPOSAL);
    myAgent.send(reply);
  }
}
