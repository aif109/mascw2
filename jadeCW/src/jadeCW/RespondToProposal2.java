package jadeCW;

import static jade.lang.acl.MessageTemplate.MatchConversationId;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import static jade.lang.acl.ACLMessage.*;
import jade.lang.acl.MessageTemplate;
import static jade.lang.acl.MessageTemplate.*;

/**
 * Behaviour for the HospitalAgent which receives a swap proposal and if the
 * proposed appointment is available handles the swap, otherwise rejecting it.
 */

// STEP 9:
public class RespondToProposal2 extends CyclicBehaviour {

  private static final long serialVersionUID = 1L;
  private static final MessageTemplate msgTemplate = and(
      MatchConversationId(Services.SWAP_APPOINTMENT),
      MatchPerformative(REQUEST));

  @Override
  public void action() {
    HospitalAgent hospitalAgent = (HospitalAgent) myAgent;
    final ACLMessage msg = myAgent.receive(msgTemplate);
    if (msg != null) {
      ACLMessage reply = msg.createReply();
      // decode swap message
      SwapMessage swpMsg = new SwapMessage(msg.getContent());
      int propApp = swpMsg.getProposedAppointment();
      int currentApp = swpMsg.getCurrentAppointment();
      String sender = msg.getSender().getLocalName();
      if (hospitalAgent.appointmentAvailable(propApp)) {
        // if proposed app. is available, handle the swap
        reply.setPerformative(ACCEPT_PROPOSAL);
        hospitalAgent.swapAppointments(propApp - 1, currentApp - 1);
        hospitalAgent.format("Swapped held app. %d with %d for '%s'",
            currentApp, propApp, sender);
      } else {
        reply.setPerformative(REJECT_PROPOSAL);
        hospitalAgent.format(
            "Cannot swap held app. %d with %d for '%s': not available!",
            currentApp, propApp, sender);
      }
      myAgent.send(reply);
    } else
      block();
  }
}
