package jadeCW;

/**
 * This class deals with the hospital query answer
 */
import static jade.lang.acl.ACLMessage.FAILURE;
import static jade.lang.acl.ACLMessage.INFORM;
import static jade.lang.acl.ACLMessage.REQUEST;
import static jade.lang.acl.MessageTemplate.MatchConversationId;
import static jade.lang.acl.MessageTemplate.MatchPerformative;
import static jade.lang.acl.MessageTemplate.and;
import jade.core.AID;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

import java.io.IOException;

//Step 6
class RespondToQuery extends CyclicBehaviour {

  private static final long serialVersionUID = 1L;

  private final HospitalAgent hospitalAgent;

  RespondToQuery(HospitalAgent hospitalAgent) {
    this.hospitalAgent = hospitalAgent;
  }

  // Receive a query regarding the owner of a certain appointment.
  private final MessageTemplate mt = and(
      MatchConversationId(Services.FIND_APPOINTMENT_OWNER),
      MatchPerformative(REQUEST));

  @Override
  public void action() {
    final ACLMessage request = this.hospitalAgent.receive(mt);

    if (request != null) {
      final ACLMessage reply = request.createReply();
      // Get the index of the appointment number
      final int appointment = Integer.parseInt(request.getContent()) - 1;

      if (!hospitalAgent.knownAppointment(appointment))
        reply.setPerformative(FAILURE);
      else {
        try {
          reply.setPerformative(INFORM);
          AID ownerAID = hospitalAgent.getOwnerAID(appointment);
          if (ownerAID == null) {
            // the appointment is not allocated to anyone, so it
            // is "owned" by the hospital agent
            ownerAID = hospitalAgent.getAID();
          }
          reply.setContentObject(ownerAID);
        } catch (final IOException e) {
          e.printStackTrace();
        }
      }
      hospitalAgent.send(reply);
    }
  }
}