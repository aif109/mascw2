package jadeCW;

/** Store for constants such as published services and conversation topics. */
public class Services {

  public static final String ALLOCATE_APPOINTMENTS = "allocate-appointments";

  public static final String FIND_APPOINTMENT_OWNER = "find-appointment-owner";

  public static final String SWAP_APPOINTMENT = "swap-appointment";

  public static final String SWAP_APPOINTMENT_NOTIF = "swap-appointment-notif";

  public static final String UPDATE = "udpate";

}
