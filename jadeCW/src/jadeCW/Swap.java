package jadeCW;

import jade.core.AID;

public class Swap implements Comparable<Swap> {

  private int appointment;
  private int priority;
  private AID agent;

  public Swap(Integer app, int priority, AID owner) {
    this.appointment = app;
    this.priority = priority;
    this.agent = owner;
  }

  @Override
  public int compareTo(Swap o) {
    return priority - o.priority;
  }

  @Override
  public boolean equals(Object o) {
    if (!(o instanceof Swap))
      return false;
    Swap s = (Swap) o;
    return appointment == s.appointment && agent.equals(s.agent);
  }

  public int getAppointment() {
    return appointment;
  }

}
