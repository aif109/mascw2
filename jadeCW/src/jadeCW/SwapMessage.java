package jadeCW;

import jade.core.AID;
import jade.lang.acl.ACLMessage;

/** Message for a proposing an appointment swap between two agents */
public class SwapMessage extends ACLMessage {

  private static final long serialVersionUID = 1L;
  private int currentAppointment;
  private int proposedAppointment;

  public SwapMessage(AID receiver, int currentAppointment,
      int proposedAppointment) {
    super(REQUEST);
    setConversationId(Services.SWAP_APPOINTMENT);
    setReplyWith(Services.SWAP_APPOINTMENT + System.currentTimeMillis());
    addReceiver(receiver);
    setContent(Integer.toString(currentAppointment) + " -> "
        + proposedAppointment);
    this.currentAppointment = currentAppointment;
    this.proposedAppointment = proposedAppointment;
  }

  public SwapMessage(String swapMessage) {
    super(REQUEST);
    this.currentAppointment = Integer.parseInt(swapMessage.split(" -> ")[0]);
    this.proposedAppointment = Integer.parseInt(swapMessage.split(" -> ")[1]);
  }

  public int getCurrentAppointment() {
    return currentAppointment;
  }

  public int getProposedAppointment() {
    return proposedAppointment;
  }

}
