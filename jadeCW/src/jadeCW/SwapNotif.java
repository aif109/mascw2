package jadeCW;

import jade.lang.acl.ACLMessage;

/** Notification message sent to Hospital agent to confirm a swap */
public class SwapNotif extends ACLMessage {
  private static final long serialVersionUID = 1L;
  private String sender;
  private String receiver;

  public SwapNotif(PatientAgent sender, String swapPartner) {
    super(INFORM);
    addReceiver(sender.getAppointmentAgent());
    setContent(sender.getLocalName() + "," + swapPartner);
    setConversationId(Services.SWAP_APPOINTMENT_NOTIF);
  }

  public SwapNotif(String msg) {
    super(INFORM);
    this.sender = msg.split(",")[0];
    this.receiver = msg.split(",")[1];
  }

  public String getReceiverAgent() {
    return receiver;
  }

  public String getSenderAgent() {
    return sender;
  }

  @Override
  public boolean equals(Object o) {
    if (!(o instanceof SwapNotif))
      return false;
    SwapNotif s = (SwapNotif) o;
    return (s.receiver.equals(receiver) && s.sender.equals(sender))
        || (s.receiver.equals(sender) && s.sender.equals(receiver));
  }

}
