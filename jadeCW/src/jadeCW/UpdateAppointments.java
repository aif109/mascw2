package jadeCW;

import static jade.lang.acl.MessageTemplate.MatchConversationId;
import static jade.lang.acl.MessageTemplate.MatchPerformative;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

import java.util.ArrayList;
import java.util.List;

/**
 * Behaviour for HospitalAgent the updates the "appointment book" when it is
 * certain that a swap has taken place. We assume that it is sufficient for both
 * parties to confirm the swap for this to be the case.
 */

// STEP 10:
public class UpdateAppointments extends CyclicBehaviour {

  private static final long serialVersionUID = 1L;
  private static final MessageTemplate msgTemplate = MessageTemplate.and(
      MatchConversationId(Services.SWAP_APPOINTMENT_NOTIF),
      MatchPerformative(ACLMessage.INFORM));
  private final List<SwapNotif> possibleSwaps = new ArrayList<SwapNotif>();

  @Override
  public void action() {
    final HospitalAgent hospitalAgent = (HospitalAgent) myAgent;
    final ACLMessage msg = myAgent.receive(msgTemplate);
    if (msg != null) {
      SwapNotif s = new SwapNotif(msg.getContent());
      if (possibleSwaps.contains(s)) {
        hospitalAgent
            .swapAppointments(s.getSenderAgent(), s.getReceiverAgent());
        possibleSwaps.remove(s);
        hospitalAgent.format(
            "Swap with '%s' confirmed by '%s'. Records updated!",
            s.getSenderAgent(), s.getReceiverAgent());
        hospitalAgent.broadcastUpdate();
      } else {
        possibleSwaps.add(s);
        hospitalAgent.format(
            "Received potential swap notification from '%s' with '%s",
            s.getSenderAgent(), s.getReceiverAgent());
      }
    } else
      block();
  }

}
