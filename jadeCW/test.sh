#!/bin/bash

TIME_LIMIT=5

# runTest(testNumber, agentConfigurationString)
runTest() {
    ulimit -t ${TIME_LIMIT}; make run "$2"  > test/test$1.out
    output=`diff test/test$1.out test/test$1.exp`
    res=$?
    if [ "$res" = "1" ]; then
        printf "=======> [FAIL!] test$1\n"
        printf " ---- Diff (meld test/$1.out test/$1.exp) ---- \n"
        printf "$output\n"
        printf " ---------------------\n"
    else
        printf "=======> [OK!]  Test Passed test$1\n"
    fi
}

make

rm -rf test/test?.out

# hospital <-> patient swap
# agent1 should request a swap with hospital agent for app. 2
runTest 1 "AGENTS=agenth:jadeCW.HospitalAgent\(3\) \
                  agent1:jadeCW.PatientAgent\(2 -\)"

# patient <-> patient swap, hospital <-> patient swap
# if agent2 gets app 1, it should request a change with the
# owner of appointment 2 (agent1). Then agent1 should request
# an appointment change for app3 from agenth.
runTest 2 "AGENTS=agenth:jadeCW.HospitalAgent\(3\) \
                  agent1:jadeCW.PatientAgent\(3 -\) \
                  agent2:jadeCW.PatientAgent\(2 -\)"

# patient <-> patient swap
# if agent1 is allocated appointment1, agent2 should try to
# swap appointment 2 with appointment 1
runTest 3 "AGENTS=agenth:jadeCW.HospitalAgent\(3\) \
                  agent1:jadeCW.PatientAgent\(1 2 3 -\) \
                  agent2:jadeCW.PatientAgent\(1 -\)"

# patient <-> patient swap
runTest 4 "AGENTS=agenth:jadeCW.HospitalAgent\(2\) \
                  agent1:jadeCW.PatientAgent\(1 -\) \
                  agent2:jadeCW.PatientAgent\(2 -\)"

# patient <-> patient swap
runTest 5 "AGENTS=agenth:jadeCW.HospitalAgent\(3\) \
                  agent1:jadeCW.PatientAgent\(1 -\) \
                  agent2:jadeCW.PatientAgent\(2 -\) \
                  agent3:jadeCW.PatientAgent\(3 -\)"

# patient <-> patient swap
runTest 6 "AGENTS=agenth:jadeCW.HospitalAgent\(2\) \
                  agent1:jadeCW.PatientAgent\(1 -\) \
                  agent2:jadeCW.PatientAgent\(2 -\) \
                  agent3:jadeCW.PatientAgent\(1 -\)"

# a larger test
runTest 7 "AGENTS=agenth:jadeCW.HospitalAgent\(10\) \
                  agent1:jadeCW.PatientAgent\(1 2 3 - 4 5 - 6 -\) \
                  agent2:jadeCW.PatientAgent\(1 - 7 8 - 9 - \) \
                  agent3:jadeCW.PatientAgent\(3 5 -\) \
                  agent4:jadeCW.PatientAgent\(1 -\) \
                  agent5:jadeCW.PatientAgent\(3 4 5 - 7 8 -\) \
                  agent6:jadeCW.PatientAgent\(3 - \) \
                  agent7:jadeCW.PatientAgent\(2 3 4 - \) \
                  agent8:jadeCW.PatientAgent\(\) \
                  agent9:jadeCW.PatientAgent\(10 - 5 6 4 - 1 2 3\) \
                  agent10:jadeCW.PatientAgent\(7) "


# patient <-> patient swap
runTest 8 "AGENTS=agenth:jadeCW.HospitalAgent\(5\) \
                  agent1:jadeCW.PatientAgent\(3 -\) \
                  agent2:jadeCW.PatientAgent\(1 -\) \
                  agent3:jadeCW.PatientAgent\(1 2 -\) \
                  agent4:jadeCW.PatientAgent\(4 -\) \
                  agent5:jadeCW.PatientAgent\(4 5 -\) \
                  agent6:jadeCW.PatientAgent\(6 -\) \
                  agent7:jadeCW.PatientAgent\(1 2 7 -\)"
